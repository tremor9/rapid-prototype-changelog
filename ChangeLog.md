# Rapid Prototype ChangeLog #

### 4/16/19 ###
With the main level completed all that is left is to spread out the buried ground,
and add the remaining results screen and final blueprints.

+Added Finish_Door blueprint
	-This blueprint will function as the exit point of the level. When the player 
	enters the door unitl trasnport to the results screena nd end the game.
	
+Added Results Screen level.
	-This will have the functionality of showing the player's final score,
	and present a nice congrats message!
	
### 4/4/19 ###
Some 'quality of life' changes have been made, As well as the main structure of the level being
almost completed for the protoype.

^Updated the coin pick-up sound effect.

+Added a Boundry trigger box
	-One part in the level is a minor jumping puzzle to get from Point A to B.	
	And this required a trigger box so if the player falls in it, they will reset
	back to the begging the puzzle.
	-Also added refrence points for the player to teleport back to when failing the puzzle.
	
+Added Jump Pads
	-Recycling from my space race protoype, These are to be used in one section
	in the level that requires big launching jump pads.
	
+Added Main Menu screen
	-With a start button & quit game button.
	
### 3/28/19 ###
I've added more terrian to the level and points of interest to make it so Buried ground
will be much easier to spot. 

+Added Moving Block blueprints
	-These function as obsticles for the player as they will keep moving 
	backwords and forwards until the level is over.
	
+Added Border walls
	-Invisible walls that will prevent the player from going out of bounds
	and stay in course with the main level.

### 3/24/19 ###
Created overview on what 'Level1' will be. It'll just be a basic level to present all the
ideas of what the platformer prototype sould be.

^Completed Sand & water biomes for the level.

+Added HUD
	-Created round timer for 3 minutes that will quit the game when it runs out.
	-Created Treasure value text to hold amount of gold/treasure collected.

### 3/20/19 ###
Begun work on overall protoype and layed out the foundation for the HUD, 
Terrain, Items/Treasure, and potienal Hazards.

+Added Basic Sand and Ocean Biomes
	-So far 20% of the wanted Terrain is complete but this is for refence for
	what the final level will look like inside the prototype.

+Added Blueprint class for 'Buried_Ground'
	-This will be what the player has to find to obtain Treasure.
	

